angular.module("dashboard").controller("researchCtrl", function($scope){
    $scope.research = [
        {
            name: "Roda",
            status: "pesquisando",
            category: "produtividade"
        },
        {
            name: "Guerreiro tribal",
            status: "habilitado",
            category: "militar"
        },
        {
            name: "Agricultura",
            status: "pesquisado",
            category: "construções"
        },
        {
            name: "Passagem de conhecimento tribal",
            status: "habilitado",
            category: "científico"
        },
    ]
})
