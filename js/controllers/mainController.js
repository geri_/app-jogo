const app = angular.module("dashboard", ['dashboard.services'])

app.run(['$rootScope', function($rootScope){
    $rootScope.apiUrl = 'http://localhost:8000'
    $rootScope.country = {
        area: 400,
        areaNow: 400,
        population: 1,
        populationNow: 1,
        maxPopulation: 1,
        populationGrowth: 0.0,
        level: "Aldeia",
        totalWorkers: 0,
        constructions: [],
        researchs: []
    }
    $rootScope.addAttr = {
        upGrowth: 0
    }
    $rootScope.wises = {
        tribalWise: {
            amount: 1,
            pointsR: 0.3
        }
    }
    $rootScope.workers = [
        {
            name: 'construtores',
            amount: 0,
            skill: 0.25
        },
        {
            name: 'caçadores',
            amount: 0,
            skill: 0.25
        },
        {
            name: 'coletores',
            amount: 0,
            skill: 0.25
        }
    ]
    $rootScope.user = {
        nome: "isario111",
        feudo: "Eisendorf",
        pontos: 0
    }
    $rootScope.military = {
        infantary: {
            amount: 20,
            helmet: null,
            breastplate: null,
            boots: null,
            pants: null,
            gauntlet: null,
            sword: null,
            shield: null,
            health: 25,
            damage: 1,
            force: 1,
            naturalDefense: 1,
            defense: 0
        }
    }
    $rootScope.resources = [
        {
            id: 1,
            name: "madeira",
            amount: 150,
            image: ''
        },
        {
            id: 2,
            name: "semente",
            amount: 0,
            image: ''
        },
        {
            id: 3,
            name: "comida",
            amount: 3000,
            image: ''
        },
        {
            id: 4,
            name: "pedra",
            amount: 0,
            image: ''
        },
        {
            id: 5,
            name: "ferramentas",
            amount: 0,
            image: ''
        }
    ]
    // 0 = Desabilitado / 1 = Habilitado / 2 = Pesquisando / 3 = Pesquisado
    $rootScope.research = [
        {
            id: 0,
            name: "Roda",
            status: 1,
            category: "produtividade",
            points: 8,
            img: '',
            attr: ['5% mais produtividade na coleta de recursos (Menos caça)','3% velocidade de construção'],
            need: ['nenhuma'],
            free: ['Ferramentas de madeira e pedra melhoradas']
        },
        {
            id: 1,
            name: "Guerreiro tribal",
            status: 1,
            category: "militar",
            points: 15,
            img: '',
            attr: ['Possibilita recrutar infantaria'],
            need: ['nenhuma'],
            free: ['Ferramentas de madeira e pedra melhoradas']
        },
        {
            id: 2,
            name: "Agricultura",
            status: 1,
            category: "construcoes",
            points: 10,
            img: '',
            attr: ['Libera construção da plantação'],
            need: ['nenhuma'],
            free: ['Trabalho com madeira']
        },
        {
            id: 3,
            name: "Passagem de conhecimento tribal",
            status: 1,
            category: "cientifico",
            points: 25,
            img: '',
            attr: ['Possibilidade de ter mais 1 sábio'],
            need: ['nenhuma'],
            free: ['nenhum']
        },
    ]
    //0 - Desabilitado / 1 - Habilitado
    $rootScope.construction = [
        {
            id: 0,
            name: 'town_hall',
            description: 'Centro da aldeia',
            img: '../assets/img/map/building/town_hall/town_hall.png',
            timeConstruction: 420000,
            level: 1,
            area: 50,
            status: 1,
            category: 'building',
            useResources: [
                {
                    name: 'madeira',
                    amount: 120
                }
            ],
            attr: [
                {
                    name: 'Ordem',
                    desc: '+1',
                }
            ],
            function: [
                {
                    functionAttr: function(){return}
                }
            ],
            production: [
                {
                    name: null,
                    amount: null
                }
            ]
        },
        {
            id: 1,
            name: 'house_tribal',
            img: '../assets/img/map/building/house/house-group-tribal2.png',
            description: 'Casa tribal',
            timeConstruction: 200000,
            level: 1,
            area: 50,
            status: 1,
            category: 'zoning',
            useResources: [
                {
                    name: 'madeira',
                    amount: 75
                }
            ],
            attr: [
                {
                    name: 'População Máxima',
                    desc: '+8',
                }
            ],
            function: [
                {
                    functionAttr: function(){
                        $rootScope.country.maxPopulation += 8
                    }
                }
            ],
            production: [
                {
                    name: null,
                    amount: null
                }
            ]
        },
        {
            id: 2,
            name: 'carpintaria',
            description: 'carpintaria',
            img: '../assets/img/map/building/carpentary/carpintaria.png',
            timeConstruction: 300000,
            level: 1,
            area: 40,
            category: 'building',
            status: 1,
            useResources: [
                {
                    name: 'madeira',
                    amount: 65
                },
                {
                    name: 'pedra',
                    amount: 10
                }
            ],
            attr: [
                {
                    name: null,
                    desc: null,
                }
            ],
            function: [
                {
                    functionAttr: function(){return}
                }
            ],
            production: [
                {
                    name: 'ferramentas',
                    amount: 5
                }
            ]
        },
        {
            id: 3,
            name: 'plantação',
            description: '',
            timeConstruction: 70000,
            level: 1,
            area: 100,
            status: 0,
            category: 'building',
            useResources: [
                {
                    name: 'madeira',
                    amount: 60
                },
                {
                    name: 'ferramentas',
                    amount: 20
                }
            ],
            attr: [
                {
                    name: 'Crescimento populacional',
                    desc: '+0.5',
                }
            ],
            function: [
                {
                    functionAttr: function(){return}
                }
            ],
            production: [
                {
                    name: 'comida',
                    amount: 5
                }
            ]
        }
    ]
}])

app.controller("mainController", ['$scope', 'timerFactory', '$interval', '$rootScope', '$timeout',function($scope, timerFactory, $interval, $rootScope, $timeout){
    
    //vars
    $scope.activeAba
    $scope.searchView
    $scope.progressSearch
    $scope.progressBuild
    $scope.researchData
    $scope.activeResearch

    //Vars preenchidas
    $scope.attrGrowth = 0
    $scope.calcGrowth = 1 + $rootScope.addAttr.upGrowth
    $scope.imgUser = '../../../../assets/img/character-avatar-icons/JPG/18.jpg'

    $scope.viewAtiva = null
    $scope.Notify = {
        ativo: false,
        img: null,
        msg: null
    }
    $scope.abrirView = function(nmView, nmAba, search) {
        $scope.viewAtiva = nmView
        $scope.activeAba = nmAba
        $scope.searchView = search
    }

    $scope.researchIdentifier = function(id) {
        var rootR = $rootScope.research[id]
        $scope.researchData = {
            id: id,
            name: rootR.name,
            img: rootR.img,
            needResearch: rootR.need,
            attr: rootR.attr,
            freeResearch: rootR.free,
            status: rootR.status
        }
        var pointsForR = $rootScope.research[id].points
        var wises = $rootScope.wises.tribalWise
        var calcTimeR = (pointsForR * 60000) / (wises.pointsR * wises.amount)
        timerFactory.timerR(calcTimeR, id)
        var RViewTime = JSON.parse(localStorage.getItem('timeResearch'))
        $scope.RViewTimeHour = RViewTime.timeBuild.hourTimer
        $scope.RViewTimeMinute = RViewTime.timeBuild.minuteTimer
        $scope.RViewTimeSecond = RViewTime.timeBuild.secondTimer
    }
    $scope.orderBy = function(campo) {
        $scope.criterion = campo
        $scope.directionFilter = !$scope.directionFilter
    }
    $scope.search = function(idResearch){
        var pointsForR = $rootScope.research[idResearch].points
        var wises = $rootScope.wises.tribalWise

        var calcTimeR = (pointsForR * 60000) / (wises.pointsR * wises.amount)

        timerFactory.timerR(calcTimeR, idResearch)
        
        if($scope.progressSearch === true){
            $scope.Notify = {
                ativo: true,
                img: '../assets/img/icons/research/wise.png',
                msg: "Já tem uma pesquisa sendo feita"
            }
            $interval(function(){$scope.Notify.ativo = false},5000,1)
        } else {
            $scope.activeResearch = $rootScope.research[idResearch].name
            $scope.progressSearch = true
            $rootScope.research[idResearch].status = 2
            $scope.RTime = JSON.parse(localStorage.getItem('timeResearch'))
            $scope.rTimeHour = $scope.RTime.timeBuild.hourTimer
            $scope.rTimeMinute = $scope.RTime.timeBuild.minuteTimer
            $scope.rTimeSecond = $scope.RTime.timeBuild.secondTimer
            $scope.Notify = {
                ativo: true,
                img: '../assets/img/icons/research/wise.png',
                msg: $rootScope.research[idResearch].name + " está sendo pesquisada"
            }
            $interval(function(){$scope.Notify.ativo = false},5000,1)
            var porMil = calcTimeR / 1000 + 1
            $interval(function(){
                if($scope.rTimeSecond > 0){$scope.rTimeSecond = Math.trunc($scope.rTimeSecond - 1);$scope.timeBuild = $scope.timeBuild - 1}
                else if ($scope.rTimeMinute > 0){$scope.rTimeMinute = Math.trunc($scope.rTimeMinute - 1);$scope.timeBuild = $scope.timeBuild - 1, $scope.rTimeSecond = 59}
                else if($scope.rTimeHour > 0){$scope.rTimeHour = Math.trunc($scope.rTimeHour - 1);$scope.timeBuild = $scope.timeBuild - 1, $scope.rTimeMinute = 59}
                else{
                    $rootScope.research[idResearch].status = 3
                    $scope.progressSearch = false
                    $scope.activeResearch = null
                    $rootScope.country.researchs.push(idResearch)
                    $scope.Notify = {
                        ativo: true,
                        img: '../assets/img/icons/research/wise.png',
                        msg: "a pesquisa " + $rootScope.research[idResearch].name + " foi concluida"
                    }
                    $interval(function(){$scope.Notify.ativo = false},5000,1)
                }
            }, 1000, porMil)
            $scope.abrirView('close','close')
        }
    }

    $scope.mapFill = function(id_building){
        if($rootScope.workers[0].amount < 1){
            $scope.Notify = {
                ativo: true, img: '../assets/img/icons/construction/hammer2.png', msg: "Você não possui " + $rootScope.workers[0].name
            }
            $interval(function(){$scope.Notify.ativo = false},5000,1)
            return
        }
        $scope.timeWork = $rootScope.construction[id_building].timeConstruction / ($rootScope.workers[0].skill * $rootScope.workers[0].amount)
        timerFactory.timer($scope.timeWork, id_building)
        var building = $rootScope.construction[id_building].name
        var usedResources = $rootScope.construction[id_building].useResources
        for(var r = 0;usedResources.length > r;r++){
            var myArray = $rootScope.resources
            var findObject = myArray.findIndex(i => i.name === usedResources[r].name);
            if(myArray[findObject].amount < usedResources[r].amount){
                $scope.Notify = {
                    ativo: true,
                    img: '../assets/img/icons/construction/hammer2.png',
                    msg: "Você não possui " + usedResources[r].name + " suficiente"
                }
                $interval(function(){$scope.Notify.ativo = false},5000,1)
                return
            } else {
                var vlrResource = myArray[findObject].amount - usedResources[r].amount
                myArray[findObject].amount = vlrResource
            }
        }
        if($scope.progressBuild === true){
            $scope.Notify = {
                ativo: true,
                img: '../assets/img/icons/construction/hammer2.png',
                msg: "Você não pode construir mais de 1 edificio por vez"
            }
            $interval(function(){$scope.Notify.ativo = false},5000,1)
            return
        } else{
            $scope.abrirView('fechar')
            $rootScope.country.areaNow = $rootScope.country.areaNow - $rootScope.construction[id_building].area
            $scope.progressBuild = true
            $scope.fTime = JSON.parse(localStorage.getItem('timeConstructions'))
            $scope.fTimeHour = $scope.fTime.timeBuild.hourTimer
            $scope.fTimeMinute = $scope.fTime.timeBuild.minuteTimer
            $scope.fTimeSecond = $scope.fTime.timeBuild.secondTimer
            $scope.timeBuild = $scope.timeWork / 1000
            var mapperView = document.getElementById('mapCountry')
            $interval(function(){
                if($scope.timeBuild > 0){
                    var divConstruction = `<div id='timerBuilding--${building}'>${$scope.fTimeHour} : ${$scope.fTimeMinute} : ${$scope.fTimeSecond}</div>`
                    angular.element(mapperView).append(divConstruction)
                    if($scope.fTimeSecond > 0){$scope.fTimeSecond = Math.trunc($scope.fTimeSecond - 1);$scope.timeBuild = $scope.timeBuild - 1}
                    else if ($scope.fTimeMinute > 0){$scope.fTimeMinute = Math.trunc($scope.fTimeMinute - 1);$scope.timeBuild = $scope.timeBuild - 1, $scope.fTimeSecond = 59}
                    else if($scope.fTimeHour > 0){$scope.fTimeHour = Math.trunc($scope.fTimeHour - 1);$scope.timeBuild = $scope.timeBuild - 1, $scope.fTimeMinute = 59}
                    $timeout(function(){
                        angular.element(document.getElementById("timerBuilding--" + building)).remove()
                    },998,$scope.timeBuild)
                } else {
                    $scope.progressBuild = false
                    var attrBuilding = $rootScope.construction[id_building].function
                    for(var b = 0;attrBuilding.length > b;b++){
                        attrBuilding[b].functionAttr()
                    }
                    $scope.Notify = {
                        ativo: true,
                        img: '../assets/img/icons/construction/hammer2.png',
                        msg: "a construção " + building.replace(/\_/g, ' ') + " foi concluida"
                    }
                    $interval(function(){$scope.Notify.ativo = false},5000,1)
                    angular.element(document.getElementById("timerBuilding--" + building)).remove()
                    divConstruction = `<div class='building--${building}'></div>`
                    angular.element(mapperView).append(divConstruction)
                }
            },1000, ($scope.timeBuild + 1))
        }
    }

    //Disable Vars
    $scope.btnWorkers = true

    //Watches
    $scope.$watch('country.maxPopulation',function(){
        if($rootScope.country.population == $rootScope.country.maxPopulation){
            $rootScope.country.populationGrowth = 0
        } else {
            $rootScope.country.populationGrowth = $scope.calcGrowth
        }
    })
    $scope.$watch('country.population',function(){
        if($rootScope.country.population == $rootScope.country.maxPopulation){
            $rootScope.country.populationGrowth = 0
        } else {
            $rootScope.country.populationGrowth = $scope.calcGrowth
        }
        $rootScope.country.populationNow = $rootScope.country.population - $rootScope.country.totalWorkers
    })    
    $scope.$watch('workers',function(){
        $scope.totalWorkers = 0
        for(var w = 0;$rootScope.workers.length > w;w++){
            if($rootScope.workers[w].amount === null || $rootScope.workers[w].amount === undefined || $rootScope.workers[w].amount < 0){
                $rootScope.workers[w].amount = 0
            } else {
                $scope.totalWorkers += parseInt($rootScope.workers[w].amount)
            }
        }
        $rootScope.country.totalWorkers = $scope.totalWorkers
        console.log($rootScope.country.totalWorkers)
        if($rootScope.country.totalWorkers > $rootScope.country.population){
            $scope.Notify = {
                ativo: true,
                img: '../assets/img/icons/construction/hammer2.png',
                msg: "Você não pode ter mais trabalhadores que população"
            }
            $interval(function(){$scope.Notify.ativo = false},5000,1)
            $scope.btnWorkers = false
        } else if ($rootScope.country.totalWorkers < 0){
            $scope.Notify = {
                ativo: true,
                img: '../assets/img/icons/construction/hammer2.png',
                msg: "Você não pode ter menos de 0 trabalhadores"
            }
            $interval(function(){$scope.Notify.ativo = false},5000,1)
            $scope.btnWorkers = false
        } else {
            $rootScope.country.populationNow = $rootScope.country.population - $rootScope.country.totalWorkers
            $scope.btnWorkers = true
        }
    }, true)

    //Timers
    $interval(function(){
        $scope.dateNow = new Date()
    }, 1000)
    $interval(function(){
        $rootScope.resources[2].amount -= $rootScope.country.population
    }, 3600000)
    $interval(function(){
        if($rootScope.country.population == $rootScope.country.maxPopulation){
            $rootScope.country.populationGrowth = 0
            return
        } else {
            $rootScope.country.population += $rootScope.country.populationGrowth
        }
    }, 360)
}])

app.controller("battleCtrl", ['$scope', '$interval', '$rootScope', function($scope, $interval, $rootScope){
    $scope.comida = $rootScope.resources[2].amount
    var infDot = $rootScope.military.infantary
    $scope.quantInfAtk = infDot.amount
    $scope.infDef = {
        attack: 1,
        defense: 1,
        life: 25
    }
    $scope.infAtk = {
        attack: infDot.damage,
        defense: infDot.defense + infDot.naturalDefense,
        life: infDot.health
    }
    console.log('infAtk: ' + $scope.infAtk)
    console.log('Quantidade Atacantes: ' + $scope.quantInfAtk)
    $scope.rodada
    $scope.calculoBtl = function(){
        if($scope.rodada === null || $scope.rodada === undefined){
            $scope.rodada = 1
        }
        $scope.deadAtkTotal = 0
        $scope.deadDefTotal = 0
        $scope.battleAtk = 0
        $scope.battleDef = 0
        $scope.troopsAliveDef = $scope.quantInfDef
        $scope.troopsAliveAtk = $scope.quantInfAtk
        $scope.rodadaNow = 1

        $interval(function(){
            if($scope.troopsAliveAtk <= 0){
                $scope.battleDef = 0
                $scope.message = 'Atacante perdeu!'
            } else if($scope.troopsAliveDef <= 0){
                $scope.battleAtk = 0
                $scope.message = 'Defensor perdeu!'
            } else if ($rootScope.resources[2].amount < $scope.troopsAliveAtk) {
                $scope.message = 'A comida acabou, partir em retirada!'
                return
            } else if($scope.troopsAliveAtk > 0 || $scope.troopsAliveDef > 0){
                $scope.damageAtk = ($scope.troopsAliveAtk * ($scope.infAtk.attack - $scope.infDef.defense / 3))
                $scope.battleAtk = (($scope.troopsAliveDef * $scope.infDef.life) - $scope.damageAtk) / $scope.infDef.life
    
                $scope.damageDef = ($scope.troopsAliveDef * ($scope.infDef.attack - $scope.infAtk.defense / 3))
                $scope.battleDef = (($scope.troopsAliveAtk * $scope.infAtk.life) - $scope.damageDef) / $scope.infAtk.life
                console.log('Rodada: ' + $scope.rodadaNow)
                console.log('Dano do Ataque: ' + $scope.damageAtk)
                console.log('Defensores vivos: ' + $scope.battleAtk)
                console.log('Dano da Defesa: ' + $scope.damageDef)
                console.log('Atacantes vivos: ' + $scope.battleDef)
                console.log('---------------------------------------------------')
                $scope.rodadaNow++
                $rootScope.resources[2].amount = $rootScope.resources[2].amount - $scope.troopsAliveAtk
                $scope.comida = $rootScope.resources[2].amount
                $scope.troopsAliveDef = $scope.quantInfDef - ($scope.quantInfDef - $scope.battleAtk)
                $scope.troopsAliveAtk = $scope.quantInfAtk - ($scope.quantInfAtk - $scope.battleDef)

                $scope.deadTroopsAtk = $scope.quantInfAtk - $scope.battleDef
                $scope.deadTroopsDef = $scope.quantInfDef - $scope.battleAtk
                $scope.deadAtkTotal = $scope.deadAtkTotal + $scope.deadTroopsAtk
                $scope.deadDefTotal = $scope.deadDefTotal + $scope.deadTroopsDef
                if($scope.troopsAliveAtk <= 0){
                    $scope.battleDef = 0
                    $scope.message = 'Atacante perdeu!'
                } else if($scope.troopsAliveDef <= 0){
                    $scope.battleAtk = 0
                    $scope.message = 'Defensor perdeu!'
                }
            }
        }, 3000, $scope.rodada)
        $scope.battleAtk = Math.trunc($scope.battleAtk)
        $scope.battleDef = Math.trunc($scope.battleDef)
    }
}])

//Filters

app.filter('capitalize', function(){
    return function(input) {
        return (angular.isString(input) && input.length > 0) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : input
    }
})