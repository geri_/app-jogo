var appService = angular.module("dashboard.services", [])

var vm = this;

// $scope.api = function(){
//     $http({
//         method: 'GET',
//         url:''
//     }).then(function successCallback(response){
        
//     }, function errorCallback(response){

//     })
// }

// ---------- Factory ----------

appService.factory('saveStorageFactory', function(){
    var _saveStorage = function(nameStorage, nameVar, tpStorage, booleanArray) {
        if(tpStorage === 0){
            if(booleanArray === 1){
                var arrayStorage = new Array()
                if(localStorage.getItem(nameStorage) === null){
                    arrayStorage.push(nameVar)
                    localStorage.setItem(nameStorage, JSON.stringify(arrayStorage))
                } else {
                    arrayStorage.push(nameVar)
                    var idVarLocal = 1
                    vm.mdamageAtkeNewVarLocal = function() {
                        if(localStorage.getItem(nameStorage + idVarLocal.toString()) === null){
                            localStorage.setItem((nameStorage + idVarLocal.toString()), JSON.stringify(arrayStorage))
                        } else {
                            idVarLocal++
                            vm.mdamageAtkeNewVarLocal()
                        }
                    }
                    vm.mdamageAtkeNewVarLocal()
                }
            } else {
                localStorage.setItem(nameStorage, JSON.stringify(nameVar))
            }
        } else {
            if(booleanArray === 1){
                var arrayStorage = new Array()
                if(sessionStorage.getItem(nameStorage) === null){
                    arrayStorage.push(nameVar)
                    sessionStorage.setItem(nameStorage, JSON.stringify(arrayStorage))
                } else {
                    arrayStorage.push(nameVar)
                    var idVarLocal = 1
                    vm.mdamageAtkeNewVarLocal = function() {
                        if(sessionStorage.getItem(nameStorage + idVarLocal.toString()) === null){
                            sessionStorage.setItem((nameStorage + idVarLocal.toString()), JSON.stringify(arrayStorage))
                        } else {
                            idVarLocal++
                            vm.mdamageAtkeNewVarLocal()
                        }
                    }
                    vm.mdamageAtkeNewVarLocal()
                }
            } else {
                localStorage.setItem(nameStorage, JSON.stringify(nameVar))
            }
        }
    }
    var functions = {
        saveStorage: _saveStorage
    }

    return functions
})

appService.factory('notificationFactory', function($interval, saveStorageFactory){
    function _researchNotification(message, researchName){
        vm.message = 'A pesquisa ' + researchName + ' ' + message
        saveStorageFactory.saveStorage('notificationResearch', vm.message, 0)
        $interval(function(){
            localStorage.removeItem('notificationResearch')
        },5000)
    }
    var notifications = {
        research: _researchNotification,
        building: _buildingNotification
    }
    return notifications
})

appService.factory('timerFactory', function($interval, saveStorageFactory){

    // function _clock(){
    //     $interval(function(){
    //         vm.dateNow = new Date()
    //         vm.hour = vm.dateNow.getHours() * 3600000
    //         vm.minute = vm.dateNow.getMinutes() * 60000
    //         vm.second = vm.dateNow.getSeconds() * 1000
    //         vm.somaTime = vm.hour + vm.minute + vm.second
    //     },1000)
    // }

    vm.stopTimer = function() {
        $interval.cancel(timerInterval)
    }

    vm.start = function() {
        vm.stopTimer()
        timerInterval = $interval(function(){
            if(vm.newTime > vm.somaTime){
                if(vm.secondTimer > 0){vm.secondTimer = Math.trunc(vm.secondTimer - 1);vm.calculoTime = vm.calculoTime - 1}
                else if (vm.minuteTimer > 0){vm.minuteTimer = Math.trunc(vm.minuteTimer - 1);vm.calculoTime = vm.calculoTime - 1, vm.secondTimer = 59}
                else if(vm.hourTimer > 0){vm.hourTimer = Math.trunc(vm.hourTimer - 1);vm.calculoTime = vm.calculoTime - 1, vm.minuteTimer = 59}
            } else {
                vm.stopTimer()
            }
        }, 1000, vm.calculoTime)
    }

    let timerInterval
    vm.timeWork

    vm.hourTimer = 0
    vm.minuteTimer = 0
    vm.secondTimer = 0
    
    function _timer(time, id) {
        vm.dateNow = new Date()
        vm.hour = vm.dateNow.getHours() * 3600000
        vm.minute = vm.dateNow.getMinutes() * 60000
        vm.second = vm.dateNow.getSeconds() * 1000
        vm.somaTime = vm.hour + vm.minute + vm.second
        vm.timeWork = time
        vm.calculoTime = vm.timeWork / 1000
        if(vm.calculoTime > 1){
            vm.newTime = parseInt(vm.somaTime) + parseInt(vm.timeWork)
            vm.finalTime = {
                second: Math.trunc((vm.newTime / 1000) % 60),
                minute: Math.trunc(((vm.newTime / 1000) / 60) % 60),
                hour: Math.trunc((vm.newTime / 1000) / 3600)
            }
            vm.timer = {
                secondTimer: Math.trunc(vm.calculoTime % 60),
                minuteTimer: Math.trunc((vm.calculoTime / 60) % 60),
                hourTimer: Math.trunc(vm.calculoTime / 3600)
            }
            vm.timeStatus = {
                timeMiliseconds: vm.newTime,
                timeCurrency: parseInt(vm.timeWork),
                finalTime: vm.finalTime,
                status: 'em andamento',
                idConstruction: id,
                timeBuild: vm.timer
            }
            saveStorageFactory.saveStorage('timeConstructions', vm.timeStatus, 0)
            console.log(timer.hourTimer + ':' + timer.minuteTimer + ':' + timer.secondTimer)
            vm.start()
            return vm.timer
        }
    }

    
    function _timerResearch(time, idResearch) {
        vm.dateNow = new Date()
        vm.hourR = vm.dateNow.getHours() * 3600000
        vm.minuteR = vm.dateNow.getMinutes() * 60000
        vm.secondR = vm.dateNow.getSeconds() * 1000
        vm.somaTimeR = vm.hour + vm.minute + vm.second
        vm.timeSearch = time
        vm.calculoTimeR = vm.timeSearch / 1000
        if(vm.calculoTimeR > 1){
            vm.newTimeR = parseInt(vm.somaTimeR) + parseInt(vm.timeSearch)
            vm.finalTimeR = {
                second: Math.trunc((vm.newTimeR / 1000) % 60),
                minute: Math.trunc(((vm.newTimeR / 1000) / 60) % 60),
                hour: Math.trunc((vm.newTimeR / 1000) / 3600)
            }
            vm.timer = {
                secondTimer: Math.trunc(vm.calculoTimeR % 60),
                minuteTimer: Math.trunc((vm.calculoTimeR / 60) % 60),
                hourTimer: Math.trunc(vm.calculoTimeR / 3600)
            }
            vm.timeStatus = {
                timeMiliseconds: vm.newTimeR,
                timeCurrency: parseInt(vm.timeSearch),
                finalTime: vm.finalTimeR,
                status: 'pesquisando',
                idResearch: idResearch,
                timeBuild: vm.timer
            }
            saveStorageFactory.saveStorage('timeResearch', vm.timeStatus, 0)
            console.log(timer.hourTimer + ':' + timer.minuteTimer + ':' + timer.secondTimer)
            vm.start()
            return vm.timer
        }
    }

    var timeServices = {
        timer: _timer,
        timerR: _timerResearch
    }
    return timeServices
})

// Services

// appServices.service('',function(){

// })